from re import I
from pandas.core.algorithms import isin
from pandas.io import sql
import xlwings as xw
import os
import redshift_connector
import pandas as pd
import yaml
import itertools
from jinjasql import JinjaSql
from pathlib import Path
from datetime import datetime
from dateutil.relativedelta import relativedelta


def sql_lead_actual():
    sql = """
    SELECT 
        lead_chegg_channel_group AS chegg_channel_group, 
		lead_course_code AS course_code,
		DATE_TRUNC('month', lead_created_dt ) AS month,
		DATE_TRUNC('week', lead_created_dt ) AS week,
		DATE_TRUNC('quarter', lead_created_dt ) AS quarter,
		SUM(lead_total) AS lead_total
    FROM "tf-dw".looker_lead_to_enroll_funnel
    WHERE lead_created_dt >= '{{lead_actual_start_dt}}'
            AND lead_created_dt < '{{forecast_start_dt}}'
            AND lead_chegg_channel_group IS NOT NULL
    GROUP BY 1, 2, 3, 4 , 5
    ORDER BY 3 DESC, 4 DESC, 1, 2
    """
    return sql


def sql_spend_actual():
    sql = """
    SELECT 
        'Paid Social' AS chegg_channel_group,
        DATE_TRUNC('month', cohort_asof ) AS month,
        DATE_TRUNC('week', cohort_asof) AS week,
        DATE_PART('month', cohort_asof) AS Mx,
        DATE_PART('week', cohort_asof) AS Wx,
        SUM(spend) AS spend 
    FROM "tf-dw".looker_ad_to_enroll_lead_cohort
    WHERE
        cohort_asof >= '{{spend_actual_start_dt}}'
        AND cohort_asof < '{{forecast_start_dt}}'
    GROUP BY 1, 2, 3, 4, 5
    UNION ALL
    SELECT 
        'Paid Search' AS chegg_channel_group,
        DATE_TRUNC('month', cohort_asof) AS month,
        DATE_TRUNC('week', cohort_asof) AS week,
        DATE_PART('month', cohort_asof) AS Mx,
        DATE_PART('week', cohort_asof) AS Wx,
        SUM(spend) AS spend
    FROM "tf-dw".looker_google_keyword_to_enroll_lead_cohort
    WHERE
        cohort_asof >= '{{spend_actual_start_dt}}'
        AND cohort_asof < '{{forecast_start_dt}}'
    GROUP BY 1, 2, 3, 4, 5
    ORDER BY 3, 5
    """
    return sql


def sql_enroll_actual():
    sql = """
    SELECT 
        CASE WHEN lead_chegg_channel_group IS NULL THEN 'Other' ELSE lead_chegg_channel_group END AS chegg_channel_group,
        DATE_TRUNC('month', lead_created_dt ) AS lead_month,
        DATE_TRUNC('month', enrollment_rain_dt) AS enroll_month,
        enrollment_course_code AS course_code,
        CASE WHEN enrollment_course_code LIKE '%%201' THEN 'flex'
            WHEN enrollment_course_code LIKE '%%301' THEN 'immersion'
            ELSE 'bloc' END AS format,
        CASE WHEN SPLIT_PART(enrollment_course_code, '-', 1) IN ('FEWD', 'DEV') THEN 'WEB_DEV'
            WHEN SPLIT_PART(enrollment_course_code, '-', 1) = 'DES' THEN 'UX'
            WHEN SPLIT_PART(enrollment_course_code, '-', 1) = 'DATA' THEN 'DATA_SCIENCE'
            ELSE SPLIT_PART(enrollment_course_code, '-', 1) END AS topic,
        enrollment_payment_plan AS payment_plan,
        SUM(rain_enrollment_total) AS enroll_total
    FROM "tf-dw".looker_enrollment
    WHERE enrollment_rain_dt IS NOT NULL
    GROUP BY 1, 2, 3, 4, 5, 6, 7
    ORDER BY enrollment_course_code
    """

    return sql


def get_ccs_range(m=3):
    wb = xw.Book.caller()
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value
    return [forecast_start_dt - relativedelta(months=x) for x in range(1, m + 1)]


def get_fx_range():
    wb = xw.Book.caller()
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value
    forecast_end_dt = wb.sheets["Control Panel"].range("b3").value
    fx_months = (
        (forecast_end_dt.year - forecast_start_dt.year) * 12
        + (forecast_end_dt.month - forecast_start_dt.month)
        + 1
    )
    return [forecast_start_dt + relativedelta(months=x) for x in range(fx_months)]


def get_finance_actuals_range():
    wb = xw.Book.caller()
    finance_start_dt = wb.sheets["Control Panel"].range("b9").value
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value

    actual_months = (forecast_start_dt.year - finance_start_dt.year) * 12 + (
        forecast_start_dt.month - finance_start_dt.month
    )
    return [finance_start_dt + relativedelta(months=x) for x in range(actual_months)]


def get_immature_lead_df():
    wb = xw.Book("pre-enroll-front-end.xlsm")
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value
    d = [
        [forecast_start_dt - relativedelta(months=x), y]
        for x, y in itertools.product([1, 2, 3, 4, 5], [5, 4, 3, 2, 1])
        if x <= y
    ]
    df = pd.DataFrame(d, columns=["lead_month", "mx"])
    return df


def add_months(start_date, delta_period):
    end_date = start_date + relativedelta(months=delta_period)
    return end_date


def get_scenario():
    wb = xw.Book.caller()
    return wb.sheets["Control Panel"].range("b7").value


def write_df_to_range(df, r, index=False, clear_all=False):
    if clear_all:
        r.sheet.clear()
    else:
        r.expand().clear()

    r.options(index=index).value = df
    r.expand().autofit()


def read_df_from_range(r, index=False):
    df = r.options(pd.DataFrame, index=index, expand="table").value
    return df


def dedupe_list(x):
    return list(dict.fromkeys(x))


def get_tfdw_conn():

    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "secrets.yaml")

    with open(filename, "r") as f:
        secrets = yaml.load(f, Loader=yaml.FullLoader)

    tfdw_user = secrets["tfdw"]["user"]
    tfdw_password = secrets["tfdw"]["password"]

    conn = redshift_connector.connect(
        host="dw-poc.crtzludxi30f.us-east-1.redshift.amazonaws.com",
        port=5439,
        database="tf-dw",
        user=tfdw_user,
        password=tfdw_password,
    )

    return conn


def filter_scenario(df, scenario):
    return df[df["scenario"] == scenario]


def clear_sheet_from_A1(name):
    wb = xw.Book.caller()
    wb.sheets[name].range("A1").expand().clear()


def lead_initialize():

    wb = xw.Book.caller()

    [
        clear_sheet_from_A1(s)
        for s in [
            "Lead Data",
            "Spend Data",
            "Organic Lead Summary",
            "Paid Lead Summary",
            "CPL CCS Adjustments",
            "Organic Lead CCS Adjustments",
            "CPL Adjustments",
            "Planned Spend",
            "Lead Distribution CCS",
            "CPL CCS",
            "Organic CCS Leads",
            "Fx CPL",
            "Fx Leads Detail",
        ]
    ]

    channels_df = read_df_from_range(wb.sheets["Control Panel"].range("channels"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))

    ccs_date_list = get_ccs_range()
    fx_date_list = get_fx_range()

    # CPL CCS Adjustments

    channel_list = channels_df.loc[channels_df["type"] == "Paid", "chegg_channel_group"]
    cpl_ccs_init_df = pd.DataFrame(
        itertools.product(channel_list, ccs_date_list),
        columns=("chegg_channel_group", "month"),
    )
    cpl_ccs_init_df["scenario"] = "baseline"
    cpl_ccs_init_df["reason"] = "This is default CCS definition"
    cpl_ccs_init_df = cpl_ccs_init_df[
        ["scenario", "chegg_channel_group", "month", "reason"]
    ]

    write_df_to_range(cpl_ccs_init_df, wb.sheets["CPL CCS Adjustments"].range("A1"))

    # Organic Lead CCS Adjustments

    channel_list = channels_df.loc[channels_df["type"] != "Paid", "chegg_channel_group"]
    organic_ccs_init_df = pd.DataFrame(
        itertools.product(channel_list, ccs_date_list),
        columns=("chegg_channel_group", "month"),
    )
    organic_ccs_init_df["scenario"] = "baseline"
    organic_ccs_init_df["reason"] = "This is default CCS definition"
    organic_ccs_init_df = organic_ccs_init_df[
        ["scenario", "chegg_channel_group", "month", "reason"]
    ]

    write_df_to_range(
        organic_ccs_init_df, wb.sheets["Organic Lead CCS Adjustments"].range("A1")
    )

    # Lead Program Distribution CCS Adjustments

    program_ccs_init_df = pd.DataFrame(
        itertools.product(
            channels_df["chegg_channel_group"],
            programs_df["course_code"],
            ccs_date_list,
        ),
        columns=("chegg_channel_group", "course_code", "month"),
    )
    program_ccs_init_df["scenario"] = "baseline"
    program_ccs_init_df["reason"] = "This is default CCS definition"
    program_ccs_init_df = program_ccs_init_df[
        ["scenario", "chegg_channel_group", "course_code", "month", "reason"]
    ].drop_duplicates()

    write_df_to_range(
        program_ccs_init_df, wb.sheets["Lead Program CCS Adjustments"].range("A1")
    )

    # CPL Adjustments

    channel_list = channels_df.loc[channels_df["type"] == "Paid", "chegg_channel_group"]
    cpl_adjustment_df = pd.DataFrame(
        itertools.product(channel_list, fx_date_list),
        columns=("chegg_channel_group", "month"),
    )
    cpl_adjustment_df["scenario"] = "baseline"
    cpl_adjustment_df["efficiency"] = 1.0
    cpl_adjustment_df["reason"] = "This is default CCS definition"
    cpl_adjustment_df = cpl_adjustment_df[
        ["scenario", "chegg_channel_group", "month", "efficiency", "reason"]
    ]

    write_df_to_range(cpl_adjustment_df, wb.sheets["CPL Adjustments"].range("A1"))

    # Planned Spend

    channel_list = channels_df.loc[channels_df["type"] == "Paid", "chegg_channel_group"]
    df = pd.DataFrame(
        itertools.product(channel_list, fx_date_list),
        columns=("chegg_channel_group", "month"),
    )
    df["scenario"] = "baseline"
    df["spend"] = 0.0
    df = df[["scenario", "chegg_channel_group", "month", "spend"]]

    write_df_to_range(df, wb.sheets["Planned Spend"].range("A1"))

    # Set baseline scenario

    wb.sheets["Control Panel"].range("B7").value = "baseline"


def lead_load():

    wb = xw.Book.caller()

    scenario = get_scenario()

    channels_df = read_df_from_range(wb.sheets["Control Panel"].range("channels"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))

    # dts suffix for date strings, dt suffix for datetime objects
    lead_actual_start_dt = wb.sheets["Control Panel"].range("b4").value
    spend_actual_start_dt = wb.sheets["Control Panel"].range("b5").value
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value
    ccs_start_dt = forecast_start_dt - relativedelta(months=3)

    lead_actual_start_dts = lead_actual_start_dt.strftime("%Y-%m-%d")
    spend_actual_start_dts = spend_actual_start_dt.strftime("%Y-%m-%d")
    forecast_start_dts = forecast_start_dt.strftime("%Y-%m-%d")
    ccs_start_dts = ccs_start_dt.strftime("%Y-%m-%d")

    conn = get_tfdw_conn()

    # Lead Data from TFDW

    params = {
        "lead_actual_start_dt": lead_actual_start_dt,
        "forecast_start_dt": forecast_start_dt,
    }

    j = JinjaSql(param_style="pyformat")
    query, bind_params = j.prepare_query(sql_lead_actual(), params)

    lead_actual_df = pd.read_sql(query % bind_params, conn)
    lead_actual_df.columns = [x.decode("utf-8") for x in lead_actual_df.columns]

    lead_actual_df = (
        pd.merge(
            lead_actual_df,
            programs_df,
            how="inner",
            left_on="course_code",
            right_on="original_course_code",
            suffixes=("_x", None),
        )
        .merge(channels_df, how="inner", on="chegg_channel_group")[
            ["chegg_channel_group", "type", "course_code", "month", "lead_total"]
        ]
        .groupby(
            ["chegg_channel_group", "type", "course_code", "month"], as_index=False
        )
        .sum()
    )

    write_df_to_range(lead_actual_df, wb.sheets["Lead Data"].range("A1"))

    ## Spend Data from TFDW

    params = {
        "spend_actual_start_dt": spend_actual_start_dt,
        "forecast_start_dt": forecast_start_dt,
    }

    j = JinjaSql(param_style="pyformat")
    query, bind_params = j.prepare_query(sql_spend_actual(), params)

    spend_actual_df = pd.read_sql(query % bind_params, conn)
    spend_actual_df.columns = [x.decode("utf-8") for x in spend_actual_df.columns]

    # Add affiliates spend from file

    filename = Path(os.path.dirname(os.path.realpath(__file__)), "affiliates.csv")
    affiliates_spend_df = pd.read_csv(filename, header=0)
    affiliates_spend_df["chegg_channel_group"] = "Affiliates"
    affiliates_spend_df["month"] = pd.to_datetime(
        affiliates_spend_df["month"], format="%Y-%m-%d"
    )
    affiliates_spend_df["week"] = None

    spend_actual_df = spend_actual_df.append(affiliates_spend_df)

    spend_actual_df = spend_actual_df[
        ["chegg_channel_group", "month", "week", "spend"]
    ].merge(channels_df, how="inner", on="chegg_channel_group")

    spend_actual_df.sort_values(
        by=["chegg_channel_group", "month", "week"], inplace=True
    )

    write_df_to_range(spend_actual_df, wb.sheets["Spend Data"].range("A1"))

    conn.close()

    ## Compute : Paid Summary (Leads, Spend, CPL by Channel and Month )

    paid_summary_df = (
        lead_actual_df[lead_actual_df["type"] == "Paid"]
        .groupby(["chegg_channel_group", "month"], as_index=False)
        .sum()
    )

    spend_summary_df = (
        spend_actual_df[["chegg_channel_group", "month", "spend"]]
        .groupby(["chegg_channel_group", "month"], as_index=False)
        .sum()
    )

    paid_summary_df = pd.merge(
        paid_summary_df,
        spend_summary_df,
        how="right",
        on=["chegg_channel_group", "month"],
    )
    paid_summary_df["cpl"] = paid_summary_df["spend"] / paid_summary_df["lead_total"]

    write_df_to_range(paid_summary_df, wb.sheets["Paid Lead Summary"].range("A1"))

    ## Compute : Organic Summary (Leads by Channel and Month)

    organic_summary_df = (
        lead_actual_df[lead_actual_df["type"] == "Organic"]
        .groupby(["chegg_channel_group", "month"], as_index=False)
        .sum()
    )

    write_df_to_range(organic_summary_df, wb.sheets["Organic Lead Summary"].range("A1"))


def lead_ccs():

    wb = xw.Book.caller()

    lead_actual_df = read_df_from_range(wb.sheets["Lead Data"].range("A1"))

    organic_summary_df = read_df_from_range(
        wb.sheets["Organic Lead Summary"].range("A1")
    )

    paid_summary_df = read_df_from_range(wb.sheets["Paid Lead Summary"].range("A1"))

    scenario = get_scenario()

    ## Compute : Lead Program Distribution CCS (Percent Leads by Program)

    lead_dist_ccs_dates_df = read_df_from_range(
        wb.sheets["Lead Program CCS Adjustments"].range("A1")
    )

    lead_dist_ccs_dates_df = lead_dist_ccs_dates_df[
        lead_dist_ccs_dates_df["scenario"] == scenario
    ]

    lead_dist_ccs_df = (
        pd.merge(
            lead_actual_df,
            lead_dist_ccs_dates_df,
            how="inner",
            on=["chegg_channel_group", "course_code", "month"],
        )
        .groupby(["chegg_channel_group", "course_code"], as_index=False)
        .sum()
    )

    lead_dist_ccs_df["percent"] = lead_dist_ccs_df[
        "lead_total"
    ] / lead_dist_ccs_df.groupby(["chegg_channel_group"])["lead_total"].transform("sum")

    lead_dist_ccs_df["scenario"] = scenario

    lead_dist_ccs_df = lead_dist_ccs_df[
        ["scenario", "chegg_channel_group", "course_code", "lead_total", "percent"]
    ]

    write_df_to_range(lead_dist_ccs_df, wb.sheets["Lead Distribution CCS"].range("A1"))

    ## Compute : Paid CPL CCS based on current scenario (CPL by Channel and Month)

    cpl_ccs_dates_df = read_df_from_range(wb.sheets["CPL CCS Adjustments"].range("A1"))

    cpl_ccs_dates_df = cpl_ccs_dates_df[cpl_ccs_dates_df["scenario"] == scenario]

    cpl_ccs_df = (
        pd.merge(
            paid_summary_df,
            cpl_ccs_dates_df,
            how="inner",
            on=["chegg_channel_group", "month"],
        )
        .groupby(["chegg_channel_group"], as_index=False)
        .mean()
    )

    cpl_ccs_df["scenario"] = scenario

    cpl_ccs_df = cpl_ccs_df[
        ["scenario", "chegg_channel_group", "lead_total", "spend", "cpl"]
    ]

    write_df_to_range(cpl_ccs_df, wb.sheets["CPL CCS"].range("A1"))

    ## Compute : Organic Leads CCS based on current scenario (Leads by Channel)

    organic_ccs_dates_df = read_df_from_range(
        wb.sheets["Organic Lead CCS Adjustments"].range("A1")
    )

    organic_ccs_dates_df = organic_ccs_dates_df[
        organic_ccs_dates_df["scenario"] == scenario
    ]

    organic_ccs_df = (
        pd.merge(
            organic_summary_df,
            organic_ccs_dates_df,
            how="inner",
            on=["chegg_channel_group", "month"],
        )
        .groupby(["chegg_channel_group"], as_index=False)
        .mean()
    )

    organic_ccs_df["scenario"] = scenario

    organic_ccs_df = organic_ccs_df[["scenario", "chegg_channel_group", "lead_total"]]

    write_df_to_range(organic_ccs_df, wb.sheets["Organic CCS Leads"].range("A1"))


def lead_forecast():

    wb = xw.Book.caller()

    ccs_date_list = get_ccs_range()
    fx_date_list = get_fx_range()

    scenario = get_scenario()

    ## Compute : Fx CPL (By Channel by fx Month)

    cpl_ccs_df = read_df_from_range(wb.sheets["CPL CCS"].range("A1"))

    cpl_ccs_adjustment_df = read_df_from_range(wb.sheets["CPL Adjustments"].range("A1"))

    cpl_ccs_adjustment_df = cpl_ccs_adjustment_df[
        cpl_ccs_adjustment_df["scenario"] == scenario
    ]

    fx_cpl_df = pd.merge(
        cpl_ccs_adjustment_df,
        cpl_ccs_df,
        how="inner",
        on="chegg_channel_group",
        suffixes=(None, "_y"),
    )

    fx_cpl_df["cpl"] = fx_cpl_df["efficiency"] * fx_cpl_df["cpl"]

    fx_cpl_df = fx_cpl_df[["scenario", "chegg_channel_group", "month", "cpl"]]

    write_df_to_range(fx_cpl_df, wb.sheets["Fx CPL"].range("A1"))

    # Fx Leads by Channel and Program

    planned_spend_df = read_df_from_range(wb.sheets["Planned Spend"].range("A1"))
    planned_spend_df = planned_spend_df[planned_spend_df["scenario"] == scenario]

    fx_leads_df = pd.merge(
        planned_spend_df,
        fx_cpl_df,
        how="inner",
        on=("chegg_channel_group", "month"),
        suffixes=(None, "_y"),
    )

    fx_leads_df["lead_total"] = fx_leads_df["spend"] / fx_leads_df["cpl"]

    fx_leads_df = fx_leads_df[
        ["scenario", "chegg_channel_group", "month", "lead_total"]
    ]

    organic_ccs_df = read_df_from_range(wb.sheets["Organic CCS Leads"].range("A1"))

    fx_date_df = pd.DataFrame(fx_date_list, columns=["month"])

    fx_organic_df = fx_date_df.merge(organic_ccs_df, how="cross")
    fx_organic_df.reset_index()

    fx_organic_df = fx_organic_df[
        ["scenario", "chegg_channel_group", "month", "lead_total"]
    ]

    fx_leads_df = pd.concat([fx_leads_df, fx_organic_df])

    fx_leads_df.sort_values(by=["chegg_channel_group", "month"], inplace=True)

    lead_program_dist_df = read_df_from_range(
        wb.sheets["Lead Distribution CCS"].range("A1")
    )

    fx_lead_program_df = pd.merge(
        fx_leads_df,
        lead_program_dist_df,
        how="inner",
        on=["chegg_channel_group"],
        suffixes=(None, "_y"),
    )

    fx_lead_program_df["lead_total"] = (
        fx_lead_program_df["lead_total"] * fx_lead_program_df["percent"]
    )

    fx_lead_program_df = fx_lead_program_df[
        ["scenario", "chegg_channel_group", "course_code", "month", "lead_total"]
    ]

    write_df_to_range(fx_lead_program_df, wb.sheets["Fx Leads Detail"].range("A1"))


def enroll_load():

    wb = xw.Book.caller()

    channels_df = read_df_from_range(wb.sheets["Control Panel"].range("channels"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))

    conn = get_tfdw_conn()

    # dts suffix for date strings, dt suffix for datetime objects
    enroll_actual_start_dt = wb.sheets["Control Panel"].range("b6").value
    forecast_start_dt = wb.sheets["Control Panel"].range("b2").value

    enroll_actual_start_dts = enroll_actual_start_dt.strftime("%Y-%m-%d")
    forecast_start_dts = forecast_start_dt.strftime("%Y-%m-%d")

    conn = get_tfdw_conn()

    # Enroll Data from TFDW

    query = sql_enroll_actual()

    enroll_df = pd.read_sql(query, conn)
    enroll_df.columns = [x.decode("utf-8") for x in enroll_df.columns]

    conn.close()

    write_df_to_range(enroll_df, wb.sheets["Enroll Data"].range("A1"))

    # Enroll Summary

    lead_actual_df = read_df_from_range(wb.sheets["Lead Data"].range("A1"))

    lead_actual_df = (
        read_df_from_range(wb.sheets["Lead Data"].range("A1"))
        .groupby(["chegg_channel_group", "course_code", "month"], as_index=False)
        .sum()
    )

    df = enroll_df

    df = df.groupby(
        ["chegg_channel_group", "course_code", "lead_month", "enroll_month"],
        as_index=False,
    )["enroll_total"].sum()

    df = pd.merge(
        lead_actual_df,
        df,
        how="left",
        left_on=("chegg_channel_group", "course_code", "month"),
        right_on=("chegg_channel_group", "course_code", "lead_month"),
    )

    df["lte"] = df["enroll_total"] / df["lead_total"]

    df.sort_values(
        by=["chegg_channel_group", "course_code", "month", "enroll_month"],
        inplace=True,
    )

    write_df_to_range(df, wb.sheets["Enroll Summary"].range("A1"))


def enroll_initialize():

    wb = xw.Book.caller()

    [
        clear_sheet_from_A1(s)
        for s in [
            "Enroll Data",
            "LTE Channel Adjustments",
            "Enroll Adjustments",
            "Enroll Summary",
            "CCS LTE",
            "CCS M Distribution",
            "Fx LTE",
            "Fx Enrolls Detail",
            "Fx Enrolls Summary",
        ]
    ]

    channels_df = read_df_from_range(wb.sheets["Control Panel"].range("channels"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))

    ccs_date_list = [d - relativedelta(months=5) for d in get_ccs_range()]
    fx_date_list = get_fx_range()

    # LTE Channel Adjustments

    channel_list = channels_df["chegg_channel_group"]
    df = pd.DataFrame(
        itertools.product(channel_list, fx_date_list),
        columns=("chegg_channel_group", "month"),
    )
    df["scenario"] = "baseline"
    df["efficiency"] = 1.0
    df = df[["scenario", "chegg_channel_group", "month", "efficiency"]]

    write_df_to_range(df, wb.sheets["LTE Channel Adjustments"].range("A1"))

    # LTE Program Adjustments

    program_list = programs_df["course_code"]

    df = pd.DataFrame(
        itertools.product(program_list, fx_date_list), columns=("course_code", "month"),
    )
    df["scenario"] = "baseline"
    df["efficiency"] = 1.0
    df = df[["scenario", "course_code", "month", "efficiency"]].drop_duplicates()

    write_df_to_range(df, wb.sheets["LTE Program Adjustments"].range("A1"))

    # Enroll Adjustments

    channel_list = channels_df["chegg_channel_group"]
    program_list = programs_df["course_code"]

    df = pd.DataFrame(
        itertools.product(channel_list, program_list, fx_date_list),
        columns=("chegg_channel_group", "course_code", "month"),
    ).drop_duplicates()

    df["scenario"] = "baseline"
    df["enrolls"] = 0
    df["efficiency"] = 1
    df = df[
        [
            "scenario",
            "chegg_channel_group",
            "course_code",
            "month",
            "enrolls",
            "efficiency",
        ]
    ]

    write_df_to_range(df, wb.sheets["Enroll Adjustments"].range("A1"))


def enroll_ccs():

    wb = xw.Book.caller()

    channels_df = read_df_from_range(wb.sheets["Control Panel"].range("channels"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))

    enroll_summary_df = read_df_from_range(wb.sheets["Enroll Summary"].range("A1"))
    lead_df = read_df_from_range(wb.sheets["Lead Data"].range("A1"))

    scenario = get_scenario()

    ccs_date_list = get_ccs_range()
    ccs_lte_dates = [
        [m, month - relativedelta(months=m), month]
        for m, month in itertools.product([0, 1, 2, 3, 4, 5], ccs_date_list)
    ]
    ccs_lte_dates_df = pd.DataFrame(
        ccs_lte_dates, columns=["mx", "lead_month", "enroll_month"]
    )

    # Compute CCS LTE by channel and program

    # Enroll totals by channel and program

    dfe = (
        enroll_summary_df.merge(
            ccs_lte_dates_df, how="inner", on=["enroll_month", "lead_month"]
        )
        .groupby(["chegg_channel_group", "course_code", "mx"], as_index=False)[
            ["enroll_total"]
        ]
        .sum()
    )

    # Leads totals by channel and program

    dfl = (
        lead_df.merge(
            ccs_lte_dates_df[["mx", "lead_month"]],
            how="inner",
            left_on="month",
            right_on="lead_month",
        )
        .groupby(["chegg_channel_group", "course_code", "mx"], as_index=False)[
            ["lead_total"]
        ]
        .sum()
    )

    # Merge lead and enrolls and compute M0, M1, M2 LTE and sum for overall LTE

    df = (
        dfl.merge(dfe, how="inner", on=["chegg_channel_group", "course_code", "mx"])
        .groupby(["chegg_channel_group", "course_code", "mx"], as_index=False)[
            ["lead_total", "enroll_total"]
        ]
        .sum()
        .assign(lte=lambda x: x.enroll_total / x.lead_total)
        .groupby(["chegg_channel_group", "course_code"], as_index=False)[["lte"]]
        .sum()
    )

    df["scenario"] = scenario

    df = df[["scenario", "chegg_channel_group", "course_code", "lte",]]

    write_df_to_range(df, wb.sheets["CCS LTE"].range("A1"))

    # Compute M0-M5 enroll distribution by program

    df = (
        enroll_summary_df.merge(
            ccs_lte_dates_df, how="inner", on=["enroll_month", "lead_month"]
        )
        .groupby(["course_code", "mx"], as_index=False)[["enroll_total"]]
        .sum()
    )

    df["percent"] = df["enroll_total"] / df.groupby("course_code")[
        "enroll_total"
    ].transform("sum")
    df["scenario"] = scenario

    df = df[["scenario", "course_code", "enroll_total", "mx", "percent",]]

    write_df_to_range(df, wb.sheets["CCS M Distribution"].range("A1"))


def enroll_forecast():

    wb = xw.Book.caller()

    scenario = get_scenario()

    #  Forecast LTE = LTE by Channel by Program by fx month with channel and program adjustments applied

    lte_channel_adj_df = filter_scenario(
        read_df_from_range(wb.sheets["LTE Channel Adjustments"].range("A1")), scenario
    )
    lte_program_adj_df = filter_scenario(
        read_df_from_range(wb.sheets["LTE Program Adjustments"].range("A1")), scenario
    )
    ccs_lte_df = filter_scenario(
        read_df_from_range(wb.sheets["CCS LTE"].range("A1")), scenario
    )
    enroll_adjustments_df = filter_scenario(
        read_df_from_range(wb.sheets["Enroll Adjustments"].range("A1")), scenario
    )

    fx_lte_df = (
        lte_channel_adj_df.merge(lte_program_adj_df, how="inner", on=["month"])
        .assign(efficiency=lambda x: x.efficiency_x * x.efficiency_y)[
            ["chegg_channel_group", "course_code", "month", "efficiency"]
        ]
        .merge(
            enroll_adjustments_df,
            how="inner",
            on=["chegg_channel_group", "course_code", "month"],
        )
        .assign(efficiency=lambda x: x.efficiency_x * x.efficiency_y)[
            ["chegg_channel_group", "course_code", "month", "efficiency"]
        ]
        .merge(ccs_lte_df, how="inner", on=["chegg_channel_group", "course_code"])
        .assign(lte=lambda x: x.lte * x.efficiency)[
            ["chegg_channel_group", "course_code", "month", "efficiency", "lte"]
        ]
        .assign(scenario=scenario)[
            [
                "scenario",
                "chegg_channel_group",
                "course_code",
                "month",
                "efficiency",
                "lte",
            ]
        ]
    )

    write_df_to_range(fx_lte_df, wb.sheets["Fx LTE"].range("A1"))

    # Forecast Enroll Detail

    fx_leads_df = read_df_from_range(wb.sheets["Fx Leads Detail"].range("A1"))

    ccs_m_dist_df = filter_scenario(
        read_df_from_range(wb.sheets["CCS M Distribution"].range("A1")), scenario
    )[["course_code", "mx", "percent"]]

    fx_enroll_df = (
        fx_leads_df.merge(
            fx_lte_df, how="inner", on=["chegg_channel_group", "course_code", "month"]
        )
        .rename(columns={"month": "lead_month"})
        .assign(enroll_total=lambda x: x.lead_total * x.lte)[
            [
                "chegg_channel_group",
                "course_code",
                "lead_month",
                "lead_total",
                "enroll_total",
                "lte",
            ]
        ]
        .merge(ccs_m_dist_df, how="inner", on=["course_code"])
        .assign(enroll_total=lambda x: x.enroll_total * x.percent, scenario=scenario,)
        .rename(columns={"enroll_total": "lte_enrolls"})
    )

    fx_enroll_df["enroll_month"] = fx_enroll_df.apply(
        lambda row: add_months(row["lead_month"], row["mx"]), axis=1
    )

    fx_enroll_df = (
        fx_enroll_df.merge(
            enroll_adjustments_df[
                ["chegg_channel_group", "course_code", "month", "enrolls"]
            ],
            how="inner",
            left_on=["chegg_channel_group", "course_code", "enroll_month"],
            right_on=["chegg_channel_group", "course_code", "month"],
        ).assign(add_enrolls=lambda x: x.enrolls)
    )[
        [
            "scenario",
            "chegg_channel_group",
            "course_code",
            "lead_month",
            "enroll_month",
            "mx",
            "lead_total",
            "lte",
            "percent",
            "lte_enrolls",
            "add_enrolls",
        ]
    ]

    fx_enroll_df.sort_values(
        by=["chegg_channel_group", "course_code", "lead_month", "enroll_month"],
        inplace=True,
    )

    write_df_to_range(fx_enroll_df, wb.sheets["Fx Enrolls Detail"].range("A1"))

    # Add enrolls from preceding months actual leads

    lead_actual_df = (
        read_df_from_range(wb.sheets["Lead Data"].range("A1"))
        .groupby(["chegg_channel_group", "course_code", "month"], as_index=False)
        .sum()
    )

    lead_immature_date_df = get_immature_lead_df()

    df_act_enrolls = lead_immature_date_df.merge(
        lead_actual_df,
        how="inner",
        left_on="lead_month",
        right_on="month",
        suffixes=("_x", None),
    )

    df_act_enrolls["enroll_month"] = df_act_enrolls.apply(
        lambda x: add_months(x["lead_month"], x["mx"]), axis=1
    )

    df_act_enrolls = (
        df_act_enrolls.merge(
            ccs_lte_df, how="inner", on=("chegg_channel_group", "course_code")
        )
        .merge(ccs_m_dist_df, how="inner", on=("course_code", "mx"))
        .assign(
            act_enrolls=lambda x: x.lead_total * x.lte * x.percent, scenario=scenario
        )
    )[
        [
            "scenario",
            "chegg_channel_group",
            "course_code",
            "lead_month",
            "enroll_month",
            "act_enrolls",
        ]
    ]

    df_act_enrolls.sort_values(
        by=["chegg_channel_group", "course_code", "enroll_month", "lead_month"],
        inplace=True,
    )

    write_df_to_range(df_act_enrolls, wb.sheets["Fx Actual Enrolls Detail"].range("A1"))

    # Combine Fx Enrolls and Fx Actual Enrolls

    df_enroll_summary = fx_enroll_df.groupby(
        ["scenario", "chegg_channel_group", "course_code", "enroll_month"],
        as_index=False,
    ).agg({"lte_enrolls": "sum", "add_enrolls": "sum"})

    df_act_enroll_summary = df_act_enrolls.groupby(
        ["scenario", "chegg_channel_group", "course_code", "enroll_month"],
        as_index=False,
    ).sum()

    df_summary_enrolls = (
        df_act_enroll_summary.merge(
            df_enroll_summary,
            how="outer",
            on=["scenario", "chegg_channel_group", "course_code", "enroll_month"],
        )
        .assign(
            enroll_total=lambda x: x[["add_enrolls", "lte_enrolls", "act_enrolls"]].sum(
                axis=1
            )
        )
        .groupby(
            ["scenario", "chegg_channel_group", "course_code", "enroll_month"],
            as_index=False,
        )
        .agg({"enroll_total": "sum"})
    )

    write_df_to_range(df_summary_enrolls, wb.sheets["Fx Enrolls Summary"].range("A1"))


def finance_init():

    wb = xw.Book.caller()

    clear_sheet_from_A1(wb.sheets["Payment Mix"])
    wb.sheets["Finance 1"].clear()
    wb.sheets["Finance 2"].clear()

    scenario = get_scenario()

    payments_df = read_df_from_range(wb.sheets["Control Panel"].range("payments"))
    programs_df = read_df_from_range(wb.sheets["Control Panel"].range("programs"))
    ccs_date_list = get_ccs_range(m=1)

    payments_ccs_df = (
        pd.DataFrame(
            itertools.product(
                programs_df["course_code"], payments_df["payment_plan"], ccs_date_list
            ),
            columns=("course_code", "payment_plan", "month"),
        )
        .drop_duplicates()
        .assign(scenario=scenario, reason="CCS Defaults")
    )[["scenario", "course_code", "payment_plan", "month", "reason"]]

    write_df_to_range(
        payments_ccs_df, wb.sheets["Payment Mix CCS Adjustments"].range("A1")
    )


def finance_output():

    wb = xw.Book.caller()

    scenario = get_scenario()

    payments_ccs_df = read_df_from_range(
        wb.sheets["Payment Mix CCS Adjustments"].range("A1")
    )
    fx_enrolls_df = read_df_from_range(wb.sheets["Fx Enrolls Summary"].range("A1"))
    act_enrolls_df = read_df_from_range(wb.sheets["Enroll Data"].range("A1"))

    fx_date_list = get_fx_range()
    act_date_list = get_finance_actuals_range()

    # Calculate payment plan mix by program

    payments_mix_df = (
        act_enrolls_df.groupby(
            ["course_code", "enroll_month", "payment_plan"], as_index=False
        )["enroll_total"]
        .sum()
        .merge(
            payments_ccs_df,
            how="inner",
            left_on=("course_code", "payment_plan", "enroll_month"),
            right_on=("course_code", "payment_plan", "month"),
        )
        .groupby(["course_code", "payment_plan"], as_index=False)["enroll_total"]
        .sum()
    )

    payments_mix_df["percent"] = payments_mix_df[
        "enroll_total"
    ] / payments_mix_df.groupby(["course_code"])["enroll_total"].transform("sum")
    payments_mix_df["scenario"] = scenario

    write_df_to_range(
        payments_mix_df[
            ["scenario", "course_code", "payment_plan", "enroll_total", "percent"]
        ],
        wb.sheets["Payment Mix"].range("A1"),
    )

    # Apply payment mix to forecast enrolls by program by payment plan

    finance1_df = (
        fx_enrolls_df.groupby(["course_code", "enroll_month"], as_index=False)[
            "enroll_total"
        ]
        .sum()
        .assign(scenario=scenario)
    )[["course_code", "enroll_month", "enroll_total"]]

    finance2_df = (
        finance1_df.merge(
            payments_mix_df, on=("course_code"), how="inner", suffixes=(None, "_y")
        )[
            [
                "scenario",
                "course_code",
                "payment_plan",
                "enroll_month",
                "enroll_total",
                "percent",
            ]
        ].assign(
            enroll_total=lambda x: x.enroll_total * x.percent
        )
    )[["course_code", "payment_plan", "enroll_month", "enroll_total"]]

    # Append actual enrolls to both

    finance1_act_df = (
        act_enrolls_df[act_enrolls_df["enroll_month"].isin(act_date_list)]
        .groupby(["course_code", "enroll_month"], as_index=False)["enroll_total"]
        .sum()
    )[["course_code", "enroll_month", "enroll_total"]]

    finance2_act_df = (
        act_enrolls_df[act_enrolls_df["enroll_month"].isin(act_date_list)]
        .groupby(["course_code", "payment_plan", "enroll_month"], as_index=False)[
            "enroll_total"
        ]
        .sum()
    )[["course_code", "payment_plan", "enroll_month", "enroll_total"]]

    # output data

    finance1_df = finance1_df.append(finance1_act_df)
    finance2_df = finance2_df.append(finance2_act_df)

    finance1_df.sort_values(["course_code", "enroll_month"], inplace=True)
    finance2_df.sort_values(
        ["course_code", "payment_plan", "enroll_month"], inplace=True
    )

    write_df_to_range(
        finance1_df.pivot_table(
            index=["course_code"], columns=["enroll_month"], values=["enroll_total"]
        ).reset_index(),
        wb.sheets["Finance 1"].range("A1"),
        clear_all=True,
    )
    write_df_to_range(
        finance2_df.pivot_table(
            index=["course_code", "payment_plan"],
            columns=["enroll_month"],
            values=["enroll_total"],
        ).reset_index(),
        wb.sheets["Finance 2"].range("A1"),
        clear_all=True,
    )


if __name__ == "__main__":
    xw.Book("pre-enroll-front-end.xlsm").set_mock_caller()
    lead_initialize()
    lead_load()
    lead_ccs()
    lead_forecast()
    enroll_initialize()
    enroll_load()
    enroll_ccs()
    enroll_forecast()
    finance_init()
    finance_output()
    # publish()

