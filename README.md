# Pre-enroll-baseline-forecast

Pre-enroll lead and enroll baseline forecasting and what-if tool

# Installation

* Install [Anaconda Individual](https://www.anaconda.com/products/individual)


* Create `forecast` conda environment
    * On Windows
        * From the Start menu, click the Anaconda Navigator desktop app
    * On Mac
        * Open a Terminal window 
    * Run following commands:

```
conda create -n forecast --clone base 
conda config --add channels conda-forge
conda install redshift_connector jinjasql -n forecast
conda activate forecast
```

* Install xlwings Excel add-in

```
xlwings addin install
```

* Clone git repo

```
git clone https://gitlab.com/mgardner1/pre-enroll-baseline-forecast.git
```
* Add secrets.yaml with tfdw creds in `\pre-enroll-baseline-forecast` folder

```
tfdw:
  user : username
  password : password
```

* Open `pre-enroll-front-end.xlsm` making sure to activate macros

* First time only
    * Mac in xlwings addin set Python Interpreter to `/Users/<username>/opt/anaconda3/envs/forecast/bin/python`
    * Windows in xlwings addin set Conda Path to `C:\Users\<username>\anaconda3` and Conda Env to `forecast`
    * Update secrets path to be the folder you stored your `secrets.yaml` file in


* `Alt-F8` to access run macros on Windows, `option-fn-F8` on Mac:
    * a1_LeadInit
    * a2_LeadLoad
    * a3_LeadCCS
    * a4_LeadFx
    * b1_EnrollInit
    * b2_EnrollLoad
    * b3_EnrollCCS
    * b4_EnrollFx




